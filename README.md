# listsync plain-HTML client

A plain HTML TODO list web site that stores your lists in a listsync back end
(such as listsync-server-rust).

## Prerequisites

[Install Rust](https://www.rust-lang.org/tools/install).

## Build

```bash
cargo test
```

## Run

Find a listsync server somewhere, or run a copy of
[listsync-server-rust](https://gitlab.com/listsync/listsync-server-rust)
locally.

```bash
cargo run
```

Now visit http://127.0.0.1:8089/

To specify where to find the server and what port to listen on, run:

```bash
cargo run -- --help
```

## License

Copyright 2020 Andy Balaam.

Released under the GNU AGPLv3, or later.  See [LICENSE](LICENSE).

Background photo by [Kolar.io](https://unsplash.com/@jankolar).

Source files inside `src/actix_session_remember` are forked from
[actis-session](https://github.com/actix/actix-web/commits/master/actix-session/src)
(at tag
[session-v0.3.0](https://github.com/actix/actix-web/releases/tag/session-v0.3.0))
- these files were released under a choice of Apache 2.0 or MIT licenses, and
the derived files are released here under the same conditions.
