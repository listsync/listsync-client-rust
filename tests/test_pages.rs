#![recursion_limit = "1024"]

use actix_web::http::header::HeaderValue;
use actix_web::http::Cookie;
use serde_json::json;
use time::Duration;
use typed_html::dom::DOMTree;
use typed_html::html;

extern crate listsync_client_rust;
mod util;

use listsync_client_rust::pages::list::process_incoming_items;

use crate::util::fake_api::FakeApi;
use crate::util::faulty_api::FaultyApi;
use crate::util::pages::{
    contact_page, delete_user_page, enter_reg_page, enter_reg_page_failure,
    enter_reg_page_must_register, list_page, login_page_failed,
    login_page_first_time, privacy_page, registered_page, rename_list_page,
    signup_page_failure, signup_page_first_time, user_exists_page, user_page,
};
use crate::util::requests::*;

#[test]
fn flow_1_requesting_root_without_cookie_redirects_to_login() {
    let api = Box::new(FakeApi::new());
    let mut app = new_app(api);
    let resp = get_nocookie(&mut app, "/");
    assert_eq!(resp.status(), 302);
    assert_contains_header(&resp, "Location", "http://localhost:8080/login");
}

#[test]
fn flow_2_login_page_presents_a_form() {
    let api = Box::new(FakeApi::new());
    let mut app = new_app(api);
    let mut resp = get_nocookie(&mut app, "/login");
    assert_eq!(fetch_body(&mut resp), login_page_first_time());
    assert_eq!(resp.status(), 200);
}

#[test]
fn flow_2_login_page_after_failure_shows_error() {
    let api = Box::new(FakeApi::new());
    let mut app = new_app(api);
    let mut resp = get_nocookie(&mut app, "/login?failed=true");
    assert_eq!(fetch_body(&mut resp), login_page_failed());
    assert_eq!(resp.status(), 200);
}

#[test]
fn flow_3a_submitting_incorrect_credentials_redirects_to_login() {
    let mut api = Box::new(FakeApi::new());
    api.add_user("a", "a");
    let mut app = new_app(api);
    let resp = post_form_no_cookie(&mut app, "/", "username=a&password=b");
    assert_eq!(resp.status(), 302);
    assert_contains_header(
        &resp,
        "Location",
        "http://localhost:8080/login?failed=true",
    );
}

#[test]
fn flow_3b_submitting_correct_credentials_provides_redirect_and_cookie() {
    let mut api = Box::new(FakeApi::new());
    api.add_user("a", "a");
    let mut app = new_app(api);

    // Correct login
    let resp = post_form_no_cookie(&mut app, "/", "username=a&password=a");

    // Redirects to user page, and provides an auth cookie
    assert_eq!(resp.status(), 302);
    assert_eq!(resp.cookies().next().unwrap().name(), "auth");
    assert_contains_header(&resp, "Location", "http://localhost:8080/list/a");
}

#[test]
fn flow_4_cookie_from_correct_login_works_to_access_page() {
    let mut api = Box::new(FakeApi::new());
    api.add_user("a", "a");
    let mut app = new_app(api);

    // Log in and get auth cookie
    let mut resp = post_form_no_cookie(&mut app, "/", "username=a&password=a");
    assert_eq!(resp.status(), 302);
    assert_contains_header(&resp, "Location", "http://localhost:8080/list/a");
    let auth_cookie: Cookie = resp.cookies().next().unwrap();
    assert_eq!(auth_cookie.name(), "auth");

    // Using the cookie works to get us the user page
    resp = get(&mut app, &auth_cookie, "/list/a");
    assert_eq!(fetch_body(&mut resp), user_page("a", vec![]));
    assert_eq!(resp.status(), 200);
}

#[test]
fn remember_me_stores_max_age_in_cookie() {
    let mut api = Box::new(FakeApi::new());
    api.add_user("a", "a");
    let mut app = new_app(api.clone());

    // When I say "remember_me=on"
    let resp = post_form_no_cookie(
        &mut app,
        "/",
        "username=a&password=a&remember_me=on",
    );

    // Then a max_age is set on my cookie
    assert_eq!(resp.status(), 302);
    let auth_cookie: Cookie = resp.cookies().next().unwrap();
    assert_eq!(auth_cookie.name(), "auth");
    assert_eq!(auth_cookie.max_age(), Some(Duration::days(30)));

    // And the API was told to remember the token
    assert_eq!(api.get_remember_me("a"), Some(true));
}

#[test]
fn remember_me_missing_does_not_store_max_age_in_cookie() {
    let mut api = Box::new(FakeApi::new());
    api.add_user("a", "a");
    let mut app = new_app(api.clone());

    // When I don't mention "remember_me"
    let resp = post_form_no_cookie(&mut app, "/", "username=a&password=a");

    // Then the max_age is not set on my cookie
    assert_eq!(resp.status(), 302);
    let auth_cookie: Cookie = resp.cookies().next().unwrap();
    assert_eq!(auth_cookie.name(), "auth");
    assert_eq!(auth_cookie.max_age(), None);

    // And the API was told not to remember the token
    assert_eq!(api.get_remember_me("a"), Some(false));
}

#[test]
fn remember_me_off_does_not_store_max_age_in_cookie() {
    let mut api = Box::new(FakeApi::new());
    api.add_user("a", "a");
    let mut app = new_app(api.clone());

    // When I say "remember_me=off"
    let resp = post_form_no_cookie(
        &mut app,
        "/",
        "username=a&password=a&remember_me=off",
    );

    // Then the max_age is not set on my cookie
    assert_eq!(resp.status(), 302);
    let auth_cookie: Cookie = resp.cookies().next().unwrap();
    assert_eq!(auth_cookie.name(), "auth");
    assert_eq!(auth_cookie.max_age(), None);

    // And the API was told not to remember the token
    assert_eq!(api.get_remember_me("a"), Some(false));
}

#[test]
fn with_no_lists_root_page_is_empty() {
    let (mut app, cookie) = logged_in_as("myuser");

    // No lists are displayed because we didn't create any
    let mut resp = get(&mut app, &cookie, "/list/myuser");
    assert_eq!(fetch_body(&mut resp), user_page("myuser", vec![]));
}

#[test]
fn user_page_lists_my_lists() {
    let (mut app, cookie) = logged_in_as("myuser");
    // Create some lists
    post_form(&mut app, &cookie, "/newlist", "title=list1");
    post_form(&mut app, &cookie, "/newlist", "title=list2");

    // They should be displayed
    let mut resp = get(&mut app, &cookie, "/list/myuser");
    assert_eq!(resp.status(), 200);
    assert_eq!(
        fetch_body(&mut resp),
        user_page("myuser", vec![("id1", "list1"), ("id2", "list2")])
    );
}

#[test]
fn empty_list_titles_are_listed_as_unnamed() {
    let (mut app, cookie) = logged_in_as("myuser");
    // Create a list with no title
    post_form(&mut app, &cookie, "/newlist", "title=");

    // It should be displayed as "(unnamed)"
    let mut resp = get(&mut app, &cookie, "/list/myuser");
    assert_eq!(resp.status(), 200);
    assert_eq!(
        fetch_body(&mut resp),
        user_page("myuser", vec![("id1", "(unnamed)")])
    );
}

#[test]
fn creating_a_new_list_redirects_to_that_list() {
    let (mut app, cookie) = logged_in_as("myuser");

    // Create a list
    let resp = post_form(&mut app, &cookie, "/newlist", "title=list1");

    // We get redirected to look at it
    assert_eq!(resp.status(), 302);
    assert_contains_header(
        &resp,
        "Location",
        "http://localhost:8080/list/myuser/id1",
    );
}

#[test]
fn renaming_a_list_displays_it_with_the_new_name() {
    // Given a list
    let (mut app, cookie) = logged_in_as("myuser");
    post_form(&mut app, &cookie, "/newlist", "title=list1");

    // When we rename it
    let mut resp =
        post_form(&mut app, &cookie, "/list/myuser/id1", "title=new%20name");

    // It is displayed with a new name
    assert_eq!(resp.status(), 200);
    let body = fetch_body(&mut resp);
    assert!(body.contains("<p>new name</p>"));
    assert!(!body.contains("<p>list1</p>"));
}

#[test]
fn list_page_shows_the_items() {
    let (mut app, cookie) = logged_in_as("myuser");
    // Create a list
    post_form(&mut app, &cookie, "/newlist", "title=list1");
    // And some items
    let mut resp = post_form(
        &mut app,
        &cookie,
        "/list/myuser/id1",
        "\
         i1_text=something%20Good&i1_itemid=0001&i1_order=16.0&\
         i2_ticked=on&i2_text=something%20bad&i2_itemid=0002&i2_order=32.0\
         ",
    );

    let exp_html: DOMTree<String> = html!(
        <div id="whole-page-div">
            <nav id="user-header-nav">
                <span id="home-span">
                    <a href="http://localhost:8080/list/myuser">
                        "\u{1F3E0} Home"
                    </a>
                </span>
                <form
                    id="log-out-form"
                    action="http://localhost:8080/login"
                    method="post"
                >
                    <button name="log_out">"Log out"</button>
                </form>
                <a href="http://localhost:8080/list/myuser">
                    "\u{1F464} myuser"
                </a>
            </nav>
            <div>
                <form
                    id="list-items-form"
                    action="http://localhost:8080/list/myuser/id1"
                    method="post"
                >
                    <p>"list1"</p>
                    <ul>
                        <li>
                            <input type="hidden" name="i1_itemid" value="0001"/>
                            <input type="hidden" name="i1_order" value="16.0"/>
                            <input type="checkbox" name="i1_ticked"/>
                            <input
                                type="text"
                                name="i1_text"
                                value="something Good"
                                placeholder="Enter a new item here"
                            />
                        </li>
                        <li>
                            <input type="hidden" name="i2_itemid" value="0002"/>
                            <input type="hidden" name="i2_order" value="32.0"/>
                            <input type="checkbox" name="i2_ticked" checked=true/>
                            <input
                                type="text"
                                name="i2_text"
                                value="something bad"
                                placeholder="Enter a new item here"
                            />
                        </li>
                        <li>
                            <input type="hidden" name="i3_itemid"/>
                            <input type="hidden" name="i3_order"/>
                            <input type="checkbox" name="i3_ticked"/>
                            <input
                                type="text"
                                name="i3_text"
                                placeholder="Enter a new item here"
                            />
                        </li>
                        <li>
                            <input type="hidden" name="i4_itemid"/>
                            <input type="hidden" name="i4_order"/>
                            <input type="checkbox" name="i4_ticked"/>
                            <input
                                type="text"
                                name="i4_text"
                                placeholder="Enter a new item here"
                            />
                        </li>
                        <li>
                            <input type="hidden" name="i5_itemid"/>
                            <input type="hidden" name="i5_order"/>
                            <input type="checkbox" name="i5_ticked"/>
                            <input
                                type="text"
                                name="i5_text"
                                placeholder="Enter a new item here"
                            />
                        </li>
                    </ul>
                    <div id="save-buttons">
                        <span id="save-to-add-more">
                            "Save to add more items"
                        </span>
                        <input
                            type="checkbox"
                            id="delete-ticked"
                            name="delete-ticked"
                        />
                        <label for="delete-ticked">
                            "Delete completed items"
                        </label>
                        <button>"Save"</button>
                    </div>
                </form>
                <form
                    id="rename-list-form"
                    class="secondary-section"
                    action="http://localhost:8080/renamelist/myuser/id1"
                    method="get"
                >
                    <button name="rename-list">"Rename list"</button>
                </form>
                <form
                    id="delete-list-form"
                    class="secondary-section"
                    action="http://localhost:8080/deletelist/myuser/id1"
                    method="post"
                >
                    <button name="delete-list">"Delete entire list"</button>
                    <input type="checkbox" name="sure" id="delete-list-sure"/>
                    <label for="delete-list-sure">"I am sure"</label>
                </form>
            </div>
        </div>
    );

    let exp_page = list_page("list1", "myuser", exp_html);

    // The initial response should show them
    assert_eq!(resp.status(), 200);
    assert_eq!(fetch_body(&mut resp), exp_page);

    // and so should a later get
    let mut resp = get(&mut app, &cookie, "/list/myuser/id1");
    assert_eq!(resp.status(), 200);
    assert_eq!(fetch_body(&mut resp), exp_page);
}

#[test]
fn when_delete_completed_items_is_ticked_they_are_not_displayed() {
    let (mut app, cookie) = logged_in_as("myuser");
    // Create a list
    post_form(&mut app, &cookie, "/newlist", "title=list1");
    // And some items, some of which are ticked
    post_form(
        &mut app,
        &cookie,
        "/list/myuser/id1",
        "\
         i1_text=KeepMe1&i1_itemid=0001&i1_order=16.0&\
         i2_ticked=on&i2_text=DeleteMe2&i2_itemid=0002&i2_order=32.0&\
         i3_ticked=on&i3_text=DeleteMe3&i3_itemid=0002&i3_order=48.0&\
         i4_text=KeepMe4&i4_itemid=0004&i4_order=64.0\
         ",
    );

    // The user ticked "Delete completed items" and then clicked Save
    let mut resp = post_form(
        &mut app,
        &cookie,
        "/list/myuser/id1",
        "\
         i1_text=KeepMe1&i1_itemid=0001&i1_order=16.0&\
         i2_ticked=on&i2_text=DeleteMe2&i2_itemid=0002&i2_order=32.0&\
         i3_ticked=on&i3_text=DeleteMe3&i3_itemid=0002&i3_order=48.0&\
         i4_text=KeepMe4&i4_itemid=0004&i4_order=64.0&\
         delete-ticked=on\
         ",
    );

    // The initial response should show only the non-ticked items
    assert_eq!(resp.status(), 200);
    let body = fetch_body(&mut resp);
    assert!(body.contains("KeepMe1"));
    assert!(body.contains("KeepMe4"));
    assert!(!body.contains("DeleteMe2"));
    assert!(!body.contains("DeleteMe3"));

    // The same is true if we GET it later
    let mut resp = get(&mut app, &cookie, "/list/myuser/id1");
    assert_eq!(resp.status(), 200);
    let body = fetch_body(&mut resp);
    assert!(body.contains("KeepMe1"));
    assert!(body.contains("KeepMe4"));
    assert!(!body.contains("DeleteMe2"));
    assert!(!body.contains("DeleteMe3"));
}

fn html_as_string(dom_tree: DOMTree<String>) -> String {
    dom_tree.to_string()
}

#[test]
fn can_delete_a_list() {
    // 2 lists exist
    let (mut app, cookie) = logged_in_as("myuser");
    post_form(&mut app, &cookie, "/newlist", "title=list1");
    post_form(&mut app, &cookie, "/newlist", "title=list2");

    // Delete one
    let resp =
        post_form(&mut app, &cookie, "/deletelist/myuser/id1", "sure=on");

    // We get redirected to user page
    assert_eq!(resp.status(), 302);
    assert_contains_header(
        &resp,
        "Location",
        "http://localhost:8080/list/myuser?message=list_deleted",
    );

    // It is gone
    let mut resp = get(&mut app, &cookie, "/list/myuser?message=list_deleted");
    assert_eq!(resp.status(), 200);
    let body = fetch_body(&mut resp);
    assert!(!body.contains("list1"));
    assert!(body.contains("list2"));
    assert!(body.contains(&html_as_string(html!(
        <div class="message">"List deleted."</div>
    ))));
}

#[test]
fn list_is_not_deleted_if_not_sure_and_error_shown() {
    // 2 lists exist
    let (mut app, cookie) = logged_in_as("myuser");
    post_form(&mut app, &cookie, "/newlist", "title=list1");
    post_form(&mut app, &cookie, "/newlist", "title=list2");

    // Try to delete one, but don't tick "I am sure"
    let resp = post_form(&mut app, &cookie, "/deletelist/myuser/list1", "");

    // We get redirected to list page
    assert_eq!(resp.status(), 302);
    assert_contains_header(
        &resp,
        "Location",
        "http://localhost:8080/list/myuser/list1?error=sure_required",
    );

    // The page we are redirected to shows an error
    let mut resp =
        get(&mut app, &cookie, "/list/myuser/id1?error=sure_required");
    assert_eq!(resp.status(), 200);
    let body = fetch_body(&mut resp);
    assert!(body.contains(&html_as_string(html!(
        <div class="error">
            r#"You must tick the "I am sure" box to delete a list."#
        </div>
    ))));
}

#[test]
fn list_is_not_deleted_if_no_cookie() {
    // 2 lists exist
    let (mut app, cookie) = logged_in_as("myuser");
    post_form(&mut app, &cookie, "/newlist", "title=list1");
    post_form(&mut app, &cookie, "/newlist", "title=list2");

    // Try to delete one with no cookie
    let resp =
        post_form_no_cookie(&mut app, "/deletelist/myuser/list1", "sure=on");

    // We get redirected to login page
    assert_eq!(resp.status(), 302);
    assert_contains_header(&resp, "Location", "http://localhost:8080/login");

    // The list is still there
    let mut resp = get(&mut app, &cookie, "/list/myuser");
    assert_eq!(resp.status(), 200);
    let body = fetch_body(&mut resp);
    assert!(body.contains("list1"));
}

#[test]
fn error_shown_on_delete_if_list_does_not_exist() {
    // 2 lists exist
    let (mut app, cookie) = logged_in_as("myuser");
    post_form(&mut app, &cookie, "/newlist", "title=list1");
    post_form(&mut app, &cookie, "/newlist", "title=list2");

    // Try to delete one with no cookie
    let resp =
        post_form(&mut app, &cookie, "/deletelist/myuser/listA", "sure=on");

    // We get an error
    assert_eq!(resp.status(), 500);
}

#[test]
fn internal_error_shown_if_api_fails() {
    let (_app, cookie) = logged_in_as("myuser");

    let faulty_api = FaultyApi {};
    let mut faulty_app = new_app(Box::new(faulty_api));

    // Attempt something using the api that fails everything
    let mut resp =
        post_form(&mut faulty_app, &cookie, "/newlist", "title=list1");

    // We get an error status
    assert_eq!(resp.status(), 500);

    // and text
    let body = fetch_body(&mut resp);
    let exp: DOMTree<String> = html!(
        <html>
            <head>
                <title>"Internal error - listsync HTML"</title>
                <meta charset="utf-8"/>
                <meta
                    content="width=device-width,initial-scale=1"
                    name="viewport"/>
                    <link
                        href="http://localhost:8080/style.css"
                        rel="stylesheet"
                    />
                </head>
            <body>
                <div id="whole-page-div">
                    <div id="internal-error-div">
                        <h1>"Internal error"</h1>
                        <div>
                            "Sorry, something went wrong.  Please try again,
                            and if that doesn't work, go back to "
                            <a href="http://localhost:8080/">
                                "\u{1F3E0} Home"
                            </a>
                            " or "
                            <a href="http://localhost:8080/contact">
                                "contact the server administrators"
                            </a>
                            "."
                        </div>
                    </div>
                </div>
            </body>
        </html>
    );

    assert_eq!(body, exp.to_string())
}

#[test]
fn form_with_extra_blank_items_is_pruned() {
    assert_eq!(
        process_incoming_items(
            vec![
                (String::from("i1_itemid"), String::from("0001")),
                (String::from("i1_order"), String::from("8.0")),
                (String::from("i1_text"), String::from("foo 1")),
                (String::from("i2_itemid"), String::from("")),
                (String::from("i2_order"), String::from("")),
                (String::from("i2_text"), String::from("")),
                (String::from("i3_itemid"), String::from("")),
                (String::from("i3_order"), String::from("")),
                (String::from("i3_text"), String::from("")),
                (String::from("i4_itemid"), String::from("0004")),
                (String::from("i4_order"), String::from("16.0")),
                (String::from("i4_ticked"), String::from("on")),
                (String::from("i4_text"), String::from("foo 4")),
                (String::from("i5_itemid"), String::from("")),
                (String::from("i5_order"), String::from("")),
                (String::from("i5_text"), String::from("")),
            ]
            .into_iter()
            .collect()
        )
        .unwrap(),
        json!([
            {
                "itemid": "0001",
                "order": 8.0,
                "ticked": false,
                "text": "foo 1"
            },
            {
                "itemid": "0004",
                "order": 16.0,
                "ticked": true,
                "text": "foo 4"
            }
        ])
    );
}

#[test]
fn rename_page_shows_name() {
    // Given a list
    let (mut app, cookie) = logged_in_as("myuser");
    post_form(&mut app, &cookie, "/newlist", "title=list1");

    // When I get the rename page
    let mut resp = get(&mut app, &cookie, "/renamelist/myuser/id1");

    // It displays as expected
    let exp_html: DOMTree<String> = html!(
        <div id="whole-page-div">
            <nav id="user-header-nav">
                <span id="home-span">
                    <a href="http://localhost:8080/list/myuser">
                        "\u{1F3E0} Home"
                    </a>
                </span>
                <form
                    id="log-out-form"
                    action="http://localhost:8080/login"
                    method="post"
                >
                    <button name="log_out">"Log out"</button>
                </form>
                <a href="http://localhost:8080/list/myuser">
                    "\u{1F464} myuser"
                </a>
            </nav>
            <div>
                <form
                    id="do-rename-list-form"
                    action="http://localhost:8080/list/myuser/id1"
                    method="post"
                >
                    <input type="text" name="title" value="list1"/>
                    <div id="rename-button">
                        <button>"Rename"</button>
                    </div>
                </form>
            </div>
        </div>
    );

    let exp_page = rename_list_page("list1", "", "myuser", exp_html);

    // The initial response should show them
    assert_eq!(resp.status(), 200);
    assert_eq!(fetch_body(&mut resp), exp_page);
}

#[test]
fn prefix_is_added_to_each_url_path() {
    // Given I have set prefix of "ui"
    let (mut app, cookie) = logged_in_with_prefix("myuser", String::from("ui"));

    // When I get a simple page
    post_form(&mut app, &cookie, "/newlist", "title=list1");
    let mut resp = get(&mut app, &cookie, "/renamelist/myuser/id1");

    // Links in the returned HTML contain the prefix
    let exp_html: DOMTree<String> = html!(
        <div id="whole-page-div">
            <nav id="user-header-nav">
                <span id="home-span">
                    <a href="http://localhost:8080/ui/list/myuser">
                        "\u{1F3E0} Home"
                    </a>
                </span>
                <form
                    id="log-out-form"
                    action="http://localhost:8080/ui/login"
                    method="post"
                >
                    <button name="log_out">"Log out"</button>
                </form>
                <a href="http://localhost:8080/ui/list/myuser">
                    "\u{1F464} myuser"
                </a>
            </nav>
            <div>
                <form
                    id="do-rename-list-form"
                    action="http://localhost:8080/ui/list/myuser/id1"
                    method="post"
                >
                    <input type="text" name="title" value="list1"/>
                    <div id="rename-button">
                        <button>"Rename"</button>
                    </div>
                </form>
            </div>
        </div>
    );
    let exp_page = rename_list_page("list1", "ui", "myuser", exp_html);
    assert_eq!(resp.status(), 200);
    assert_eq!(fetch_body(&mut resp), exp_page);
}

#[test]
fn logging_out_clears_auth_cookie_and_deletes_token() {
    // Given I am logged in, and my cookie is not empty
    let api = Box::new(FakeApi::new());
    let (mut app, cookie) = logged_in_with_api("myuser", api.clone());
    assert_eq!(cookie.name(), "auth");
    assert_ne!(cookie.value(), "");

    // And I click "Log out"
    let resp = post_form(&mut app, &cookie, "/login", "");
    assert_eq!(200, resp.status());
    let new_cookie = resp.cookies().next().unwrap().into_owned();

    // The session cookie has been wiped
    assert_eq!(new_cookie.name(), "auth");
    assert_eq!(new_cookie.value(), "");
    assert_eq!(new_cookie.max_age(), Some(Duration::seconds(0)));

    // And the token was deleted
    assert_eq!(api.get_deleted_tokens("myuser"), vec!["myuser-token"])
}

#[test]
fn signup_flow_1_signup_page_presents_a_form() {
    let api = Box::new(FakeApi::new());
    let mut app = new_app(api);
    let mut resp = get_nocookie(&mut app, "/signup");
    assert_eq!(fetch_body(&mut resp), signup_page_first_time());
    assert_eq!(resp.status(), 200);
}

#[test]
fn signup_flow_2a_submitting_incorrect_user_details_displays_error() {
    let api = Box::new(FakeApi::new());
    let mut app = new_app(api);
    let mut resp = post_form_no_cookie(
        &mut app,
        "/signup",
        "username=newuser&password=ps&password_repeat=WRONG",
    );
    assert_eq!(fetch_body(&mut resp), signup_page_failure());
    assert_eq!(resp.status(), 200);
}

#[test]
fn signup_flow_2b_submitting_existing_user_details_displays_error() {
    let (mut app, _cookie) = logged_in_as("myuser");
    let mut resp = post_form_no_cookie(
        &mut app,
        "/signup",
        "username=myuser&password=ps&password_repeat=ps",
    );
    assert_eq!(fetch_body(&mut resp), user_exists_page());
    assert_eq!(resp.status(), 200);
}

#[test]
fn signup_flow_2c_submitting_unregistered_user_details_displays_error() {
    // Given an unregistered user
    let (mut app, _cookie) = logged_in_as("myuser");
    post_form_no_cookie(
        &mut app,
        "/signup",
        "username=unr&password=ps&password_repeat=ps",
    );

    // When I try to sign up again as them, we get a "user exists" page
    let mut resp = post_form_no_cookie(
        &mut app,
        "/signup",
        "username=unr&password=ps&password_repeat=ps",
    );
    assert_eq!(fetch_body(&mut resp), user_exists_page());
    assert_eq!(resp.status(), 200);
}

#[test]
fn signup_flow_2_submitting_user_details_displays_registration_page() {
    let api = Box::new(FakeApi::new());
    let mut app = new_app(api);
    let mut resp = post_form_no_cookie(
        &mut app,
        "/signup",
        "username=newuser&password=ps&password_repeat=ps",
    );
    assert_eq!(fetch_body(&mut resp), enter_reg_page("newuser", "reg1"));
    assert_eq!(resp.status(), 200);
}

#[test]
fn signup_flow_3a_submitting_incorrect_registration_code_shows_error() {
    // Sign up
    let api = Box::new(FakeApi::new());
    let mut app = new_app(api);
    post_form_no_cookie(
        &mut app,
        "/signup",
        "username=newuser&password=ps&password_repeat=ps",
    );

    // Try to register, with wrong code
    let mut resp = post_form_no_cookie(
        &mut app,
        "/signup/newuser",
        "registration_code=WRONG&real_registration_code=reg1",
    );
    assert_eq!(
        fetch_body(&mut resp),
        enter_reg_page_failure("newuser", "reg1")
    );
    assert_eq!(resp.status(), 200);
}

#[test]
fn signup_flow_3b_logging_in_when_unregistered_asks_to_register() {
    // Sign up
    let api = Box::new(FakeApi::new());
    let mut app = new_app(api);
    post_form_no_cookie(
        &mut app,
        "/signup",
        "username=newuser&password=ps&password_repeat=ps",
    );

    // Try to log in
    let mut resp =
        post_form_no_cookie(&mut app, "/", "username=newuser&password=ps");

    // Registration page is shown
    assert_eq!(
        fetch_body(&mut resp),
        enter_reg_page_must_register("newuser", "reg1")
    );
    assert_eq!(resp.status(), 200);
}

#[test]
fn signup_flow_3_submitting_registration_code_shows_success() {
    // Sign up
    let api = Box::new(FakeApi::new());
    let mut app = new_app(api);
    post_form_no_cookie(
        &mut app,
        "/signup",
        "username=newuser&password=ps&password_repeat=ps",
    );

    // Register with correct code
    let mut resp = post_form_no_cookie(
        &mut app,
        "/signup/newuser",
        "registration_code=reg1&real_registration_code=reg1",
    );
    assert_eq!(fetch_body(&mut resp), registered_page());
    assert_eq!(resp.status(), 200);
}

#[test]
fn contact_page_displays() {
    let api = Box::new(FakeApi::new());
    let mut app = new_app(api);
    let mut resp = get_nocookie(&mut app, "/contact");
    assert_eq!(fetch_body(&mut resp), contact_page());
    assert_eq!(resp.status(), 200);
}

#[test]
fn privacy_page_displays() {
    let api = Box::new(FakeApi::new());
    let mut app = new_app(api);
    let mut resp = get_nocookie(&mut app, "/privacy");
    assert_eq!(fetch_body(&mut resp), privacy_page());
    assert_eq!(resp.status(), 200);
}

#[test]
fn can_change_password() {
    let (mut app, cookie) = logged_in_as("myuser");
    let mut resp = post_form(
        &mut app,
        &cookie,
        "/list/myuser",
        "password=pwn&password_repeat=pwn",
    );

    // We get a success response
    assert_eq!(resp.status(), 200);
    assert!(fetch_body(&mut resp).contains("Password changed."));

    // We can log in with the new password
    let resp =
        post_form_no_cookie(&mut app, "/", "username=myuser&password=pwn");
    assert_eq!(resp.status(), 302);
    assert_eq!(
        resp.headers().get("location"),
        Some(&HeaderValue::from_static(
            "http://localhost:8080/list/myuser"
        ))
    );

    // And we can't with the old
    let resp =
        post_form_no_cookie(&mut app, "/", "username=myuser&password=pw");
    assert_eq!(resp.status(), 302);
    assert_eq!(
        resp.headers().get("location"),
        Some(&HeaderValue::from_static(
            "http://localhost:8080/login?failed=true"
        ))
    );
}

#[test]
fn changing_password_with_non_matching_passwords_fails() {
    let (mut app, cookie) = logged_in_as("myuser");
    let mut resp = post_form(
        &mut app,
        &cookie,
        "/list/myuser",
        "password=pwn&password_repeat=AAA",
    );

    // We get a failure message
    assert_eq!(resp.status(), 400);
    assert!(fetch_body(&mut resp).contains("Passwords didn&#x27;t match!"));

    // We can't log in with the new password
    let resp =
        post_form_no_cookie(&mut app, "/", "username=myuser&password=pwn");
    assert_eq!(resp.status(), 302);
    assert_eq!(
        resp.headers().get("location"),
        Some(&HeaderValue::from_static(
            "http://localhost:8080/login?failed=true"
        ))
    );

    // But we can with old
    let resp =
        post_form_no_cookie(&mut app, "/", "username=myuser&password=pw");
    assert_eq!(resp.status(), 302);
    assert_eq!(
        resp.headers().get("location"),
        Some(&HeaderValue::from_static(
            "http://localhost:8080/list/myuser"
        ))
    );
}

#[test]
fn changing_password_with_empty_password_fails() {
    let (mut app, cookie) = logged_in_as("myuser");
    let mut resp = post_form(
        &mut app,
        &cookie,
        "/list/myuser",
        "password=&password_repeat=",
    );

    // We get a failure message
    assert_eq!(resp.status(), 400);
    assert!(fetch_body(&mut resp).contains("Passwords can&#x27;t be empty!"));

    // We can't log in with the empty password
    let resp = post_form_no_cookie(&mut app, "/", "username=myuser&password=");
    assert_eq!(resp.status(), 302);
    assert_eq!(
        resp.headers().get("location"),
        Some(&HeaderValue::from_static(
            "http://localhost:8080/login?failed=true"
        ))
    );

    // But we can with old
    let resp =
        post_form_no_cookie(&mut app, "/", "username=myuser&password=pw");
    assert_eq!(resp.status(), 302);
    assert_eq!(
        resp.headers().get("location"),
        Some(&HeaderValue::from_static(
            "http://localhost:8080/list/myuser"
        ))
    );
}

#[test]
fn delete_user_flow_1a_not_sure_shows_message() {
    let (mut app, cookie) = logged_in_as("myuser");

    // Try to delete without ticking sure
    let resp = post_form(&mut app, &cookie, "/deleteuser/myuser", "");

    // We get redirected
    assert_eq!(resp.status(), 302);
    assert_eq!(
        resp.headers().get("location"),
        Some(&HeaderValue::from_static(
            "http://localhost:8080/list/myuser?error=sure_required"
        ))
    );

    // And the resulting page shows an error
    let mut resp = get(
        &mut app,
        &cookie,
        "http://localhost:8080/list/myuser?error=sure_required",
    );
    assert_eq!(resp.status(), 200);
    assert!(fetch_body(&mut resp).contains(
        r#"<div class="error">You must choose &quot;I am sure&quot;.</div>"#
    ));
}

#[test]
fn delete_user_flow_1_sure_shows_delete_user_page() {
    let (mut app, cookie) = logged_in_as("myuser");

    // Ask to delete a user
    let mut resp =
        post_form(&mut app, &cookie, "/deleteuser/myuser", "sure=on");

    // We get asked one more time
    assert_eq!(resp.status(), 200);
    assert_eq!(fetch_body(&mut resp), delete_user_page("myuser"));
}

#[test]
fn delete_user_flow_2a_not_really_sure_shows_message() {
    let (mut app, cookie) = logged_in_as("myuser");

    // Try to delete without ticking really sure
    let mut resp = post_form(
        &mut app,
        &cookie,
        "/deleteuser/myuser",
        "sure=on&really_sure=",
    );

    // We are reminded just how sure we need to be
    assert_eq!(resp.status(), 200);
    assert!(fetch_body(&mut resp).contains(&format!(
        "{}{}",
        r#"<div class="error">Confirm by choosing "#,
        r#"&quot;I am REALLY sure&quot;.</div>"#
    )));
}

#[test]
fn delete_user_flow_2b_no_really_sure_shows_message() {
    let (mut app, cookie) = logged_in_as("myuser");

    // Try to delete without ticking really sure
    let mut resp =
        post_form(&mut app, &cookie, "/deleteuser/myuser", "sure=on");

    // We are reminded just how sure we need to be
    assert_eq!(resp.status(), 200);
    assert!(fetch_body(&mut resp).contains(&format!(
        "{}{}",
        r#"<div class="error">Confirm by choosing "#,
        r#"&quot;I am REALLY sure&quot;.</div>"#
    )));
}

#[test]
fn delete_user_flow_2_user_is_gone_and_redirected_to_login() {
    let (mut app, cookie) = logged_in_as("myuser");

    // Ask to delete
    let resp = post_form(
        &mut app,
        &cookie,
        "/deleteuser/myuser",
        "sure=on&really_sure=on",
    );

    // Get redirected to login page
    assert_eq!(resp.status(), 302);
    assert_eq!(
        resp.headers().get("location"),
        Some(&HeaderValue::from_static(
            "http://localhost:8080/login?message=user_deleted"
        ))
    );

    // And the resulting page shows it was deleted
    let mut resp = get(
        &mut app,
        &cookie,
        "http://localhost:8080/login?message=user_deleted",
    );
    assert_eq!(resp.status(), 200);
    assert!(fetch_body(&mut resp).contains(
        r#"<p class="login-message">User deleted.  Please log in:</p>"#
    ));
}
