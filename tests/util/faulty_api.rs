use async_trait::async_trait;

use listsync_client_rust::api::{Api, ApiError};
use listsync_client_rust::credentials::Credentials;

pub struct FaultyApi {}

#[async_trait]
impl Api for FaultyApi {
    async fn get(
        &self,
        _credentials: &Credentials,
        _path: &str,
    ) -> Result<serde_json::Value, ApiError> {
        Err(ApiError::InternalError)
    }

    async fn get_no_credentials(
        &self,
        _path: &str,
    ) -> Result<serde_json::Value, ApiError> {
        Err(ApiError::InternalError)
    }

    async fn put(
        &self,
        _credentials: &Credentials,
        _path: &str,
        _body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        Err(ApiError::InternalError)
    }

    async fn put_no_credentials(
        &self,
        _path: &str,
        _body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        Err(ApiError::InternalError)
    }

    async fn post(
        &self,
        _credentials: &Credentials,
        _path: &str,
        _body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        Err(ApiError::InternalError)
    }

    async fn post_no_credentials(
        &self,
        _path: &str,
        _body: serde_json::Value,
    ) -> Result<serde_json::Value, ApiError> {
        Err(ApiError::InternalError)
    }

    async fn delete(
        &self,
        _credentials: &Credentials,
        _path: &str,
        _body: Option<serde_json::Value>,
    ) -> Result<serde_json::Value, ApiError> {
        Err(ApiError::InternalError)
    }
}
