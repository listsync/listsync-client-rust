#![recursion_limit = "1024"] // Needed for typed_html

use actix_web::web;

pub mod actix_session_remember;
pub mod api;
pub mod contact_page;
pub mod cookie_session;
pub mod credentials;
pub mod data_holder;
mod domtree_responder;
mod insert_domtree;
mod login_credentials;
pub mod lsc_error;
mod page;
pub mod pages;
pub mod real_api;
pub mod urls;

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::resource("/favicon.ico")
            .name("favicon_ico")
            .route(web::get().to(pages::favicon_ico::get)),
    )
    .service(
        web::resource("/kolar-io-ICOdHM1Cuvg-unsplash.jpg")
            .name("background_image")
            .route(
                web::get().to(pages::kolar_io_icodhm1cuvg_unsplash_jpg::get),
            ),
    )
    .service(
        web::resource("/style.css")
            .name("style_css")
            .route(web::get().to(pages::style_css::get)),
    )
    .service(
        web::resource("/contact")
            .name("contact")
            .route(web::get().to(pages::contact::get)),
    )
    .service(
        web::resource("/privacy")
            .name("privacy")
            .route(web::get().to(pages::privacy::get)),
    )
    .service(
        web::resource("/login")
            .name("login")
            .route(web::get().to(pages::login::get))
            .route(web::post().to(pages::login::post)),
    )
    .service(
        web::resource("/newlist")
            .name("newlist")
            .route(web::post().to(pages::newlist::post)),
    )
    .service(
        web::resource("/renamelist/{username}/{listid}")
            .name("renamelist")
            .route(web::get().to(pages::renamelist::get)),
    )
    .service(
        web::resource("/signup")
            .name("signup")
            .route(web::get().to(pages::signup::get))
            .route(web::post().to(pages::signup::post)),
    )
    .service(
        web::resource("/signup/{username}")
            .name("signup_user")
            .route(web::post().to(pages::signup::post_user)),
    )
    .service(
        web::resource("/deletelist/{username}/{listid}")
            .name("deletelist")
            .route(web::post().to(pages::deletelist::post)),
    )
    .service(
        web::resource("/deleteuser/{username}")
            .name("deleteuser")
            .route(web::post().to(pages::deleteuser::post)),
    )
    .service(
        web::resource("/list/{username}/{listid}")
            .name("list")
            .route(web::get().to(pages::list::get))
            .route(web::post().to(pages::list::post)),
    )
    .service(
        web::resource("/list/{username}")
            .name("user")
            .route(web::get().to(pages::user::get))
            .route(web::post().to(pages::user::post)),
    )
    .service(
        web::resource("/")
            .name("root")
            .route(web::get().to(pages::root::get))
            .route(web::post().to(pages::root::post)),
    );
}
