use actix_web::{http, HttpResponse};

pub async fn get() -> HttpResponse {
    HttpResponse::Ok()
        .content_type("image/x-icon")
        .header(http::header::CACHE_CONTROL, "max-age=2419200")
        .body(include_bytes!("favicon.ico") as &'static [u8])
}
