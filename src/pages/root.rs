use actix_web::Responder;
use actix_web::{http, web, FromRequest, HttpRequest, HttpResponse};
use typed_html::html;

use crate::credentials::Credentials;
use crate::data_holder::DataHolder;
use crate::login_credentials::LoginCredentials;
use crate::lsc_error::LscError;
use crate::pages::signup;
use crate::urls::Urls;

pub async fn post(
    req: HttpRequest,
    urls: Urls,
    login_credentials: web::Form<LoginCredentials>,
    data_holder: web::Data<DataHolder>,
) -> Result<HttpResponse, LscError> {
    let res = Credentials::save(&req, &urls, &login_credentials).await;
    match res {
        Ok(_) => get(req, urls).await,
        Err(LscError::Forbidden(_)) => {
            let reg = fetch_registration_code(
                &req,
                &urls,
                &data_holder,
                &login_credentials.username,
            )
            .await;
            match reg {
                Ok(registration_code) => {
                    display_register_page_on_root(
                        &req,
                        &urls,
                        &login_credentials.username,
                        &registration_code,
                    )
                    .await
                }
                Err(e) => Err(e),
            }
        }
        Err(e) => Err(e),
    }
}

async fn display_register_page_on_root(
    req: &HttpRequest,
    urls: &Urls,
    username: &str,
    registration_code: &str,
) -> Result<HttpResponse, LscError> {
    signup::display_register_page(
        Some(html!(
            <div id="failure">"You must register before you can log in."</div>
        )),
        req,
        urls,
        signup::RegisteredUser {
            username: String::from(username),
            registration_code: String::from(registration_code),
        },
    )
    .respond_to(&req)
    .await
    .map_err(|_| {
        LscError::internal_error(
            &req,
            &urls,
            "Failed to convert display_register_page to HTTP response.",
        )
    })
}

async fn fetch_registration_code(
    req: &HttpRequest,
    urls: &Urls,
    data_holder: &DataHolder,
    username: &str,
) -> Result<String, LscError> {
    let url = format!("/v1/newuser/{}", username);
    data_holder
        .api
        .get_no_credentials(&url)
        .await
        .map_err(|e| LscError::from_api_error(req, urls, e))
        .and_then(|reg_code_json| {
            reg_code_json
                .get("registration_code")
                .ok_or(LscError::internal_error(
                    req,
                    urls,
                    "Failed to get reg code from signup/username response.",
                ))
                .and_then(|value| {
                    value.as_str().ok_or(LscError::internal_error(
                        req,
                        urls,
                        "Registration code was not a string.",
                    ))
                })
                .map(String::from)
        })
}

pub async fn get(
    req: HttpRequest,
    urls: Urls,
) -> Result<HttpResponse, LscError> {
    match Credentials::extract(&req, &urls).await {
        Ok(credentials) => {
            validate_login(&req, &urls, &credentials).await.map(|_| {
                HttpResponse::Found()
                    .header(
                        http::header::LOCATION,
                        urls.url_for(&req, "user", vec![credentials.username])
                            .unwrap()
                            .to_string(),
                    )
                    .finish()
                    .into_body()
            })
        }
        Err(e) => Err(e),
    }
}

async fn validate_login(
    req: &HttpRequest,
    urls: &Urls,
    credentials: &Credentials,
) -> Result<(), LscError> {
    let data_holder = web::Data::<DataHolder>::extract(&req).await.unwrap();
    let url = format!("/v1/list/{}", credentials.username);
    data_holder
        .api
        .get(&credentials, &url)
        .await
        .map_err(|e| LscError::from_api_error(req, urls, e))
        .map(|_| ())
}
