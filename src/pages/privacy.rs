use actix_web::{web, HttpRequest};
use typed_html::{html, unsafe_text};

use crate::data_holder::DataHolder;
use crate::domtree_responder::DOMTreeResponder;
use crate::lsc_error::LscError;
use crate::page::page;
use crate::urls::Urls;

pub async fn get(
    req: HttpRequest,
    urls: Urls,
    data_holder: web::Data<DataHolder>,
) -> Result<DOMTreeResponder, LscError> {
    // Note: no credentials needed for this page.
    data_holder
        .api
        .get_no_credentials("/v1/privacy")
        .await
        .map_err(|e| LscError::from_api_error(&req, &urls, e))
        .and_then(|privacy_json| {
            privacy_json
                .as_str()
                .ok_or_else(|| {
                    LscError::internal_error(
                        &req,
                        &urls,
                        "Privacy response was not a string.",
                    )
                })
                .map(|privacy_html| {
                    page(
                        "Privacy",
                        None,
                        html!(
                            <div class="info-container-div">
                                <h1>"Privacy"</h1>
                                <div class="info-div">
                                    {unsafe_text!(privacy_html)}
                                </div>
                            </div>
                        ),
                        &req,
                        &urls,
                    )
                    .into()
                })
        })
}
