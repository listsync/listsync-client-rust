use actix_web::{web, HttpRequest};
use serde::{Deserialize, Serialize};
use typed_html::elements::{div, p};
use typed_html::{html, text};

use crate::api::ApiError;
use crate::data_holder::DataHolder;
use crate::domtree_responder::DOMTreeResponder;
use crate::lsc_error::LscError;
use crate::page::page;
use crate::urls::Urls;

#[derive(Deserialize)]
pub struct SignupCredentials {
    pub username: String,
    pub password: String,
    pub password_repeat: String,
}

#[derive(Serialize)]
pub struct NewUser {
    pub username: String,
    pub password: String,
}

impl From<&SignupCredentials> for NewUser {
    fn from(signup_credentials: &SignupCredentials) -> NewUser {
        NewUser {
            username: String::from(&signup_credentials.username),
            password: String::from(&signup_credentials.password),
        }
    }
}

#[derive(Deserialize)]
pub struct RegisteredUser {
    pub username: String,
    pub registration_code: String,
}

#[derive(Deserialize, Serialize)]
pub struct RegistrationCode {
    pub registration_code: String,
    pub real_registration_code: String,
}

#[derive(Deserialize)]
pub struct UserPath {
    pub username: String,
}

pub async fn post_user(
    path: web::Path<UserPath>,
    req: HttpRequest,
    urls: Urls,
    data_holder: web::Data<DataHolder>,
    registration_code: web::Form<RegistrationCode>,
) -> DOMTreeResponder {
    register_user(&path.username, &data_holder, &registration_code)
        .await
        .map(|_| display_registration_success(&req, &urls))
        .unwrap_or_else(|_| {
            display_register_page(
                Some(html!(
                    <div id="failure">
                        "The registration code you typed was incorrect.  "
                        "Please try again."
                    </div>
                )),
                &req,
                &urls,
                RegisteredUser {
                    username: String::from(&path.username),
                    registration_code: String::from(
                        &registration_code.real_registration_code,
                    ),
                },
            )
        })
}

fn display_registration_success(
    req: &HttpRequest,
    urls: &Urls,
) -> DOMTreeResponder {
    page(
        "Registration succeeded",
        None,
        html!(
            <p class="log-in-link-div"><span>
                "Registration succeeded.  "
                <a href={
                    urls.url_for_static(&req, "login").unwrap().to_string()
                }>
                    "Log in"
                </a>
                " to get started!"
            </span></p>
        ),
        &req,
        &urls,
    )
    .into()
}

fn parse_registered_user(
    req: &HttpRequest,
    urls: &Urls,
    user_json: serde_json::Value,
) -> Result<RegisteredUser, LscError> {
    serde_json::from_value(user_json).map_err(|_| {
        LscError::internal_error(&req, &urls, "Failed to parse RegisteredUser")
    })
}

fn process_signup_error(
    e: ApiError,
    req: &HttpRequest,
    urls: &Urls,
) -> Result<DOMTreeResponder, LscError> {
    match e {
        ApiError::UserAlreadyExists => Ok(display_signup_page(
            &req,
            &urls,
            html!(
                <p id="failure">
                    "Someone has already taken that username.  "
                    "Please try another."
                </p>
            ),
        )),
        _ => Err(LscError::from_api_error(&req, &urls, e)),
    }
}

pub async fn post(
    req: HttpRequest,
    urls: Urls,
    data_holder: web::Data<DataHolder>,
    signup_credentials: web::Form<SignupCredentials>,
) -> Result<DOMTreeResponder, LscError> {
    if signup_credentials.password == signup_credentials.password_repeat {
        create_user(&data_holder, &signup_credentials)
            .await
            .map_or_else(
                |e| process_signup_error(e, &req, &urls),
                |user_json| {
                    parse_registered_user(&req, &urls, user_json).map(
                        |registered_user| {
                            display_register_page(
                                None,
                                &req,
                                &urls,
                                registered_user,
                            )
                        },
                    )
                },
            )
    } else {
        Ok(display_signup_page(
            &req,
            &urls,
            html!(
                <p id="failure">
                    "The passwords you typed do not match.  Please try again."
                </p>
            ),
        ))
    }
}

pub async fn get(req: HttpRequest, urls: Urls) -> DOMTreeResponder {
    display_signup_page(
        &req,
        &urls,
        html!(
            <p class="login-message">
                "Sign up to start making lists!"
            </p>
        ),
    )
}

async fn register_user(
    username: &str,
    data_holder: &DataHolder,
    registration_code: &RegistrationCode,
) -> Result<(), ApiError> {
    let url = format!("/v1/newuser/{}", username);
    data_holder
        .api
        .put_no_credentials(
            &url,
            serde_json::to_value(registration_code)
                .expect("Unable to serialize RegistrationCode)"),
        )
        .await
        .map(|_| ())
    // TODO: map UserAlreadyRegistered and IncorrectRegistrationCode to
    //       a different response from some other API error.
}

async fn create_user(
    data_holder: &DataHolder,
    signup_credentials: &SignupCredentials,
) -> Result<serde_json::Value, ApiError> {
    data_holder
        .api
        .post_no_credentials(
            "/v1/newuser",
            serde_json::to_value(NewUser::from(signup_credentials))
                .expect("Unable to serialize UpdatedList"),
        )
        .await
}

fn display_signup_page(
    req: &HttpRequest,
    urls: &Urls,
    message: Box<p<String>>,
) -> DOMTreeResponder {
    page(
        "Sign up",
        None,
        html!(
            <div>
                <div class="login-div">
                    {message}
                    <form
                        action={
                            urls
                                .url_for_static(&req, "signup")
                                .unwrap()
                                .to_string()
                        }
                        method="post"
                    >
                        <label for="username">"Username "</label>
                        <input
                            id="username"
                            name="username"
                            placeholder="Enter a username e.g. andyb"
                        />
                        <label for="password">"Password "</label>
                        <input
                            id="password"
                            name="password"
                            placeholder="Enter a password"
                            type="password"
                        />
                        <label for="password">"Password (repeat) "</label>
                        <input
                            id="password_repeat"
                            name="password_repeat"
                            placeholder="Repeat your password"
                            type="password"
                        />
                        <button>"Sign up"</button>
                    </form>
                    <p id="cookie-warning">
                        "This site uses a single authentication "
                        <a href="https://en.wikipedia.org/wiki/HTTP_cookie">
                            "cookie"
                        </a>
                        " to enable logging in.  It is not used to track any"
                        " information about you."
                    </p>
                </div>
                <div class="secondary-section log-in-link-div">
                    <span>
                        "Been here before? "
                        <a href={
                            urls
                                .url_for_static(&req, "login")
                                .unwrap()
                                .to_string()
                            }>
                            "Log in"
                        </a>
                        "."
                    </span>
                </div>
            </div>
        ),
        &req,
        &urls,
    )
    .into()
}

pub fn display_register_page(
    error_message: Option<Box<div<String>>>,
    req: &HttpRequest,
    urls: &Urls,
    registered_user: RegisteredUser,
) -> DOMTreeResponder {
    let error_message = if let Some(e) = error_message {
        e
    } else {
        html!(<div></div>)
    };
    let message: Box<p<String>> = html!(
        <p class="login-message">
            "Your registration code is "
            <span id="reg-code">{
                text!(&registered_user.registration_code)
            }</span>
            ".  Please enter it below."
        </p>
    );
    page(
        "Enter registration code",
        None,
        html!(
            <div>
                <div class="login-div">
                    {error_message}
                    {message}
                    <form
                        action={
                            urls.url_for(
                                req,
                                "signup_user",
                                vec![registered_user.username]
                            ).unwrap().to_string()
                        }
                        method="post"
                    >
                        <label for="registration_code">
                            "Registration code "
                        </label>
                        <input
                            id="registration_code"
                            name="registration_code"
                            placeholder="Enter the code e.g. dF-9H"
                        />
                        <input
                            id="real_registration_code"
                            name="real_registration_code"
                            type="hidden"
                            value={&registered_user.registration_code}
                        />
                        <button>"Submit"</button>
                    </form>
                </div>
            </div>
        ),
        &req,
        &urls,
    )
    .into()
}
