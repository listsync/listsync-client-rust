use actix_web::{web, HttpRequest};
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};
use typed_html::dom::{DOMTree, UnsafeTextNode};
use typed_html::types::Id;
use typed_html::{html, text, unsafe_text};

use crate::credentials::Credentials;
use crate::data_holder::DataHolder;
use crate::domtree_responder::DOMTreeResponder;
use crate::lsc_error::LscError;
use crate::page::page;
use crate::urls::Urls;

#[derive(Deserialize)]
pub struct Path {
    username: String,
    listid: String,
}

#[derive(Clone, Deserialize, Serialize)]
pub struct ListItem {
    pub itemid: String,
    pub order: f64,
    pub ticked: bool,
    pub text: String,
}

#[derive(Deserialize)]
pub struct ListInfo {
    pub listid: String,
    pub title: String,
}

#[derive(Serialize)]
struct UpdatedList {
    pub title: String,
}

pub async fn get(
    req: HttpRequest,
    urls: Urls,
    path: web::Path<Path>,
    data_holder: web::Data<DataHolder>,
) -> Result<DOMTreeResponder, LscError> {
    match Credentials::extract(&req, &urls).await {
        Ok(credentials) => {
            show_list_page(&req, &urls, &path, &data_holder, &credentials).await
        }
        Err(e) => Err(e),
    }
}

pub async fn post(
    req: HttpRequest,
    urls: Urls,
    path: web::Path<Path>,
    data_holder: web::Data<DataHolder>,
    payload: web::Form<HashMap<String, String>>,
) -> Result<DOMTreeResponder, LscError> {
    match Credentials::extract(&req, &urls).await {
        Ok(credentials) => {
            if let Some(new_title) = payload.get("title") {
                rename_list_and_show(
                    &req,
                    &urls,
                    &path,
                    &data_holder,
                    &credentials,
                    new_title,
                )
                .await
            } else {
                let list_info = fetch_list_info(
                    &req,
                    &urls,
                    &path,
                    &data_holder,
                    &credentials,
                )
                .await?;

                save_list_items_and_show(
                    &req,
                    &urls,
                    &path,
                    req.query_string(),
                    &data_holder,
                    &credentials,
                    list_info,
                    payload.into_inner(),
                )
                .await
            }
        }
        Err(e) => Err(e),
    }
}

async fn rename_list_and_show(
    req: &HttpRequest,
    urls: &Urls,
    path: &Path,
    data_holder: &DataHolder,
    credentials: &Credentials,
    new_title: &str,
) -> Result<DOMTreeResponder, LscError> {
    let url = format!("/v1/list/{}/{}", path.username, path.listid);
    data_holder
        .api
        .put(
            &credentials,
            &url,
            serde_json::to_value(UpdatedList {
                title: String::from(new_title),
            })
            .expect("Unable to serialize UpdatedList"),
        )
        .await
        .map_err(|e| LscError::from_api_error(req, urls, e))?;

    // TODO: we ignore the response from the api here, which is a list_info
    show_list_page(req, urls, path, data_holder, credentials).await
}

fn next_id(all_ids: &HashSet<String>) -> String {
    let max_numeric_id: i32 = all_ids
        .iter()
        .map(|itemid| {
            let ret: i32 = itemid.parse().unwrap_or(0);
            ret
        })
        .max()
        .unwrap_or(0);
    format!("{:04}", (max_numeric_id + 1))
}

fn unique_id(
    all_ids: &HashSet<String>,
    proposed_id: Option<&String>,
) -> String {
    match proposed_id {
        Some(proposed_id) => {
            if all_ids.contains(proposed_id) || proposed_id == "" {
                next_id(all_ids)
            } else {
                String::from(proposed_id)
            }
        }
        None => next_id(all_ids),
    }
}

fn new_item(
    form_map: &HashMap<String, String>,
    i: usize,
    text: &str,
    all_ids: &HashSet<String>,
    cur_order: f64,
) -> ListItem {
    let ti = format!("i{}_ticked", i);
    let or = format!("i{}_order", i);
    let id = format!("i{}_itemid", i);
    let mut order = form_map
        .get(&or)
        .map(|order| order.parse::<f64>().unwrap_or(cur_order + 16.0))
        .unwrap_or(cur_order + 16.0);
    if order <= cur_order {
        order = cur_order + 16.0
    }
    ListItem {
        text: String::from(text),
        ticked: form_map.get(&ti).map(|s| s == "on").unwrap_or(false),
        itemid: unique_id(all_ids, form_map.get(&id)),
        order,
    }
}

pub fn process_incoming_items(
    form_map: HashMap<String, String>,
) -> Result<serde_json::Value, serde_json::error::Error> {
    let delete_completed =
        form_map.get("delete-ticked") == Some(&String::from("on"));
    let mut all_ids: HashSet<String> = HashSet::new();
    let mut res: Vec<ListItem> = vec![];
    let mut cur_order = 0.0;
    for i in 1.. {
        let te = format!("i{}_text", i);
        match form_map.get(&te) {
            Some(text) => {
                if text != "" {
                    let item =
                        new_item(&form_map, i, text, &all_ids, cur_order);
                    if !delete_completed || !item.ticked {
                        all_ids.insert(String::from(&item.itemid));
                        cur_order = item.order;
                        res.push(item)
                    }
                }
            }
            None => break,
        }
    }
    serde_json::to_value(res)
}

async fn fetch_list_info(
    req: &HttpRequest,
    urls: &Urls,
    path: &Path,
    data_holder: &DataHolder,
    credentials: &Credentials,
) -> Result<ListInfo, LscError> {
    // TODO: add an API that gets list and its items in 1 fetch?
    let list_url = format!("/v1/list/{}/{}", path.username, path.listid);
    data_holder
        .api
        .get(&credentials, &list_url)
        .await
        .map_err(|e| LscError::from_api_error(req, urls, e))
        .and_then(|list_json| {
            serde_json::from_value(list_json).map_err(|_| {
                LscError::internal_error(req, urls, "Failed to parse ListInfo")
            })
        })
}

async fn save_list_items_and_show(
    req: &HttpRequest,
    urls: &Urls,
    path: &Path,
    query: &str,
    data_holder: &DataHolder,
    credentials: &Credentials,
    list_info: ListInfo,
    payload: HashMap<String, String>,
) -> Result<DOMTreeResponder, LscError> {
    let url = format!("/v1/list/{}/{}/items", path.username, path.listid);
    match process_incoming_items(payload) {
        Ok(items) => data_holder
            .api
            .put(&credentials, &url, items)
            .await
            .map_err(|e| LscError::from_api_error(req, urls, e))
            .and_then(|res| {
                display_items(
                    path,
                    query,
                    credentials,
                    list_info,
                    res,
                    req,
                    urls,
                )
            }),
        Err(_) => Err(LscError::internal_error(
            req,
            urls,
            "Failed to process incoming items",
        )),
    }
}

async fn show_list_page(
    req: &HttpRequest,
    urls: &Urls,
    path: &Path,
    data_holder: &DataHolder,
    credentials: &Credentials,
) -> Result<DOMTreeResponder, LscError> {
    let list_info =
        fetch_list_info(req, urls, path, data_holder, credentials).await?;

    let items_url = format!("/v1/list/{}/{}/items", path.username, path.listid);
    data_holder
        .api
        .get(&credentials, &items_url)
        .await
        .map_err(|e| LscError::from_api_error(req, urls, e))
        .and_then(|api_response| {
            display_items(
                path,
                req.query_string(),
                credentials,
                list_info,
                api_response,
                req,
                urls,
            )
        })
}

fn display_items(
    path: &Path,
    query: &str,
    credentials: &Credentials,
    list_info: ListInfo,
    api_response: serde_json::Value,
    req: &HttpRequest,
    urls: &Urls,
) -> Result<DOMTreeResponder, LscError> {
    serde_json::from_value::<Vec<ListItem>>(api_response)
        .map_err(|_| {
            LscError::internal_error(
                req,
                urls,
                "Failed to parse list of ListItem",
            )
        })
        .map(|items| {
            show_list_page_html(
                path,
                credentials,
                query,
                list_info,
                items,
                req,
                urls,
            )
        })
}

fn json_f64(num: f64) -> String {
    if num == std::f64::MIN {
        String::from("")
    } else {
        serde_json::to_string(&serde_json::Number::from_f64(num))
            .unwrap_or(String::from("0.0"))
    }
}

fn plus_three_empty_items(
    items: Vec<ListItem>,
) -> impl Iterator<Item = (usize, ListItem)> {
    items
        .into_iter()
        .chain((0..3).map(|_| ListItem {
            itemid: String::from(""),
            order: std::f64::MIN,
            ticked: false,
            text: String::from(""),
        }))
        .enumerate()
}

fn errors(query: &str) -> Option<DOMTree<String>> {
    if query == "error=sure_required" {
        Some(html!(
            <div class="error">
                r#"You must tick the "I am sure" box to delete a list."#
            </div>
        ))
    } else {
        None
    }
}

fn display_error(query: &str) -> Box<UnsafeTextNode<String>> {
    unsafe_text!(errors(query)
        .map(|e| e.to_string())
        .unwrap_or(String::from("")))
}

fn show_list_page_html(
    path: &Path,
    credentials: &Credentials,
    query: &str,
    list_info: ListInfo,
    items: Vec<ListItem>,
    req: &HttpRequest,
    urls: &Urls,
) -> DOMTreeResponder {
    page(
        &list_info.title,
        Some(&credentials.username),
        html!(
            <div>
                {display_error(query)}
                <form
                    id="list-items-form"
                    action={
                        urls.url_for(
                            req,
                            "list",
                            vec![&path.username, &path.listid]
                        ).unwrap().to_string()
                    }
                    method="post"
                >
                    <p>{text!(&list_info.title)}</p>
                    <ul>{
                        plus_three_empty_items(items).map(|(zero_index, item)| {
                            let index = zero_index + 1;
                            let id = Id::new(format!("i{}_itemid", index));
                            let or = Id::new(format!("i{}_order", index));
                            let ti = Id::new(format!("i{}_ticked", index));
                            let te = Id::new(format!("i{}_text", index));
                            html!( <li>
                                <input
                                    type="hidden" name=id value=&item.itemid/>
                                <input
                                    type="hidden"
                                    name=or
                                    value={json_f64(item.order)}
                                />
                                {
                                    if item.ticked {
                                        html!(
                                            <input
                                                type="checkbox"
                                                name=ti
                                                checked=true
                                            />
                                        )
                                    } else {
                                        html!(<input type="checkbox" name=ti/>)
                                    }
                                }
                                <input
                                    type="text"
                                    name=te
                                    value=&item.text
                                    placeholder="Enter a new item here"
                                />
                            </li>)
                        })
                    }</ul>
                    <div id="save-buttons">
                        <span id="save-to-add-more">
                            "Save to add more items"
                        </span>
                        <input
                            type="checkbox"
                            id="delete-ticked"
                            name="delete-ticked"
                        />
                        <label for="delete-ticked">
                            "Delete completed items"
                        </label>
                        <button>"Save"</button>
                    </div>
                </form>
                <form
                    id="rename-list-form"
                    class="secondary-section"
                    action={
                        urls.url_for(
                            req,
                            "renamelist",
                            vec![&path.username, &path.listid]
                        ).unwrap().to_string()
                    }
                    method="get"
                >
                    <button name="rename-list">"Rename list"</button>
                </form>
                <form
                    id="delete-list-form"
                    class="secondary-section"
                    action={
                        urls.url_for(
                            req,
                            "deletelist",
                            vec![&path.username, &path.listid]
                        ).unwrap().to_string()
                    }
                    method="post"
                >
                    <button name="delete-list">"Delete entire list"</button>
                    <input type="checkbox" name="sure" id="delete-list-sure"/>
                    <label for="delete-list-sure">"I am sure"</label>
                </form>
            </div>
        ),
        req,
        urls,
    )
    .into()
}
