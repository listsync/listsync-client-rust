use crate::api::Api;
use crate::contact_page::ContactPage;
use crate::urls::Urls;

pub struct DataHolder {
    pub api: Box<dyn Api>,
    pub contact_page: ContactPage,
    pub urls: Urls,
}

impl DataHolder {
    pub fn new(
        api: Box<dyn Api>,
        contact_page: ContactPage,
        urls: Urls,
    ) -> DataHolder {
        DataHolder {
            api,
            contact_page,
            urls,
        }
    }
}
