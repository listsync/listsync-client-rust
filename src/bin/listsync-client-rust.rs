use actix_web::{middleware, App, HttpServer};

use listsync_client_rust;
use listsync_client_rust::contact_page::ContactPage;
use listsync_client_rust::data_holder::DataHolder;
use listsync_client_rust::real_api::RealApi;
use listsync_client_rust::urls::Urls;

struct Args {
    server_url: String,
    bind: String,
    port: String,
    path_prefix: Option<String>,
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();

    let contact_page = ContactPage::load("contact.html");
    let args = cmdline_args();
    let server_url = String::from(args.server_url);
    let path_prefix: Option<String> = args.path_prefix.clone();

    println!("Connecting to server at {}", server_url);
    println!("Listening on http://{}:{}", args.bind, args.port);
    HttpServer::new(move || {
        App::new()
            .wrap(middleware::NormalizePath)
            .data(DataHolder::new(
                Box::new(RealApi::new(server_url.clone())),
                contact_page.clone(),
                Urls::new(path_prefix.clone()),
            ))
            .wrap(listsync_client_rust::cookie_session::new_session())
            .wrap(middleware::Logger::default())
            .configure(listsync_client_rust::config)
    })
    .bind(format!("{}:{}", args.bind, args.port))?
    .run()
    .await
}

fn cmdline_args() -> Args {
    let matches = clap::App::new("listsync-client-rust")
        .version(&clap::crate_version!()[..])
        .about(
            "A web interface for editing lists stored in a listsync server. \
             See https://gitlab.com/listsync/",
        )
        .arg(
            clap::Arg::with_name("server_url")
                .long("server_url")
                .value_name("URL")
                .help(
                    "Specify server to connect to \
                     [default: http://localhost:8088]",
                ),
        )
        .arg(
            clap::Arg::with_name("bind")
                .long("bind")
                .value_name("ADDRESS")
                .help(
                    "Specify alternate bind address [default: all interfaces]",
                ),
        )
        .arg(
            clap::Arg::with_name("PORT")
                .index(1)
                .help("Specify alternate port [default: 8089]"),
        )
        .arg(
            clap::Arg::with_name("path-prefix")
                .long("path-prefix")
                .value_name("PREFIX")
                .help(
                    "Specify a prefix to add to each path.  If you are running
                    the server inside a subdirectory via a proxy, you should
                    supply the subdirectory name here.
                    [default: no prefix is added]",
                ),
        )
        .get_matches();

    Args {
        server_url: String::from(
            matches
                .value_of("server_url")
                .unwrap_or("http://localhost:8088"),
        ),
        bind: String::from(matches.value_of("bind").unwrap_or("0.0.0.0")),
        port: String::from(matches.value_of("PORT").unwrap_or("8089")),
        path_prefix: matches.value_of("path-prefix").map(String::from),
    }
}
