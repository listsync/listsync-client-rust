use actix_web::HttpRequest;
use typed_html::dom::DOMTree;
use typed_html::elements::nav;
use typed_html::{html, text};

use crate::insert_domtree::insert_domtree;
use crate::urls::Urls;

pub fn page(
    title: &str,
    username: Option<&str>,
    content: DOMTree<String>,
    req: &HttpRequest,
    urls: &Urls,
) -> DOMTree<String> {
    html!(
        <html>
            <head>
                <title>{text!("{} - listsync HTML", title)}</title>
                <meta charset="utf-8"/>
                <meta
                    name="viewport"
                    content="width=device-width,initial-scale=1"
                />
                <link
                    href={
                        urls
                            .url_for_static(req, "style_css")
                            .unwrap()
                            .to_string()
                        }
                    rel="stylesheet"
                />
            </head>
            <body>
                <div id="whole-page-div">
                    {
                        if let Some(u) = username {
                            user_header_nav(u, req, urls)
                        } else {
                            plain_header_nav(req, urls)
                        }
                    }
                    {insert_domtree(content)}
                </div>
                <footer>
                    <p>
                        <a href=
                            "https://gitlab.com/listsync/listsync-client-rust"
                        >
                            "listsync HTML"
                        </a>
                        " is Free Software, released under the AGPLv3."
                    </p>
                    <p>
                        "Find out how to "
                        <a href={
                            urls
                                .url_for_static(req, "contact")
                                .unwrap()
                                .to_string()
                        }>
                            "contact the server admins"
                        </a>
                        " or "
                        <a href={
                            urls
                                .url_for_static(req, "privacy")
                                .unwrap()
                                .to_string()
                        }>
                            "read the privacy policy"
                        </a>
                        "."
                    </p>
                </footer>
            </body>
        </html>
    )
}

fn user_header_nav(
    username: &str,
    req: &HttpRequest,
    urls: &Urls,
) -> Box<nav<String>> {
    let home_url = urls
        .url_for(req, "user", vec![username])
        .unwrap()
        .to_string();
    html!(
        <nav id="user-header-nav">
            <span id="home-span"><a href=&home_url>"\u{1F3E0} Home"</a></span>
            <form
                id="log-out-form"
                action={
                    urls.url_for_static(&req, "login").unwrap().to_string()
                }
                method="post"
            >
                <button name="log_out">"Log out"</button>
            </form>
            <a href=&home_url>{text!("\u{1F464} {}", username)}</a>
        </nav>
    )
}

fn plain_header_nav(req: &HttpRequest, urls: &Urls) -> Box<nav<String>> {
    html!(
        <nav id="plain-header-nav">
            <a href={urls.url_for_static(req, "root").unwrap().to_string()}>
                "\u{1F3E0} Home"
            </a>
        </nav>
    )
}
