--- cookie_session-v0.3.0.rs	2022-01-02 09:56:42.971403657 +0000
+++ cookie.rs	2020-04-23 08:36:47.712487046 +0100
@@ -28,7 +28,7 @@
 use futures::future::{ok, FutureExt, LocalBoxFuture, Ready};
 use serde_json::error::Error as JsonError;
 
-use crate::{Session, SessionStatus};
+use crate::actix_session_remember::{Session, SessionStatus};
 
 /// Errors that can occur during handling cookie session
 #[derive(Debug, From, Display)]
@@ -77,10 +77,27 @@
         }
     }
 
+    fn new_cookie(&self, value: String) -> Cookie<'static> {
+        let mut cookie = Cookie::new(self.name.clone(), value);
+        cookie.set_path(self.path.clone());
+        cookie.set_secure(self.secure);
+        cookie.set_http_only(self.http_only);
+
+        if let Some(ref domain) = self.domain {
+            cookie.set_domain(domain.clone());
+        }
+
+        if let Some(same_site) = self.same_site {
+            cookie.set_same_site(same_site);
+        }
+        cookie
+    }
+
     fn set_cookie<B>(
         &self,
         res: &mut ServiceResponse<B>,
         state: impl Iterator<Item = (String, String)>,
+        remember_me: Option<bool>,
     ) -> Result<(), Error> {
         let state: HashMap<String, String> = state.collect();
         let value = serde_json::to_string(&state)
@@ -89,21 +106,21 @@
             return Err(CookieSessionError::Overflow.into());
         }
 
-        let mut cookie = Cookie::new(self.name.clone(), value);
-        cookie.set_path(self.path.clone());
-        cookie.set_secure(self.secure);
-        cookie.set_http_only(self.http_only);
+        let mut cookie = self.new_cookie(value);
 
-        if let Some(ref domain) = self.domain {
-            cookie.set_domain(domain.clone());
-        }
-
-        if let Some(max_age) = self.max_age {
-            cookie.set_max_age(max_age);
-        }
-
-        if let Some(same_site) = self.same_site {
-            cookie.set_same_site(same_site);
+        if let Some(remember_me) = remember_me {
+            // We were explicitly told to remember this user
+            if remember_me {
+                // TODO: hard-coded to 30 days
+                cookie.set_max_age(time::Duration::days(30));
+            } else {
+                // TODO: no way to UNset max age on a cookie!
+            }
+        } else {
+            if let Some(max_age) = self.max_age {
+                // The default was set when we created the CookieSession
+                cookie.set_max_age(max_age);
+            }
         }
 
         let mut jar = CookieJar::new();
@@ -126,8 +143,7 @@
         &self,
         res: &mut ServiceResponse<B>,
     ) -> Result<(), Error> {
-        let mut cookie = Cookie::named(self.name.clone());
-        cookie.set_value("");
+        let mut cookie = self.new_cookie(String::from(""));
         cookie.set_max_age(time::Duration::seconds(0));
         cookie.set_expires(time::now() - time::Duration::days(365));
 
@@ -193,7 +209,7 @@
 /// # Example
 ///
 /// ```rust
-/// use actix_session::CookieSession;
+/// use listsync_client_rust::actix_session_remember::CookieSession;
 /// use actix_web::{web, App, HttpResponse, HttpServer};
 ///
 /// fn main() {
@@ -340,23 +356,31 @@
         async move {
             fut.await.map(|mut res| {
                 match Session::get_changes(&mut res) {
-                    (SessionStatus::Changed, Some(state))
-                    | (SessionStatus::Renewed, Some(state)) => {
-                        res.checked_expr(|res| inner.set_cookie(res, state))
-                    }
-                    (SessionStatus::Unchanged, _) =>
+                    (
+                        SessionStatus::Changed,
+                        Some(state),
+                        Some(remember_me),
+                    )
+                    | (
+                        SessionStatus::Renewed,
+                        Some(state),
+                        Some(remember_me),
+                    ) => res.checked_expr(|res| {
+                        inner.set_cookie(res, state, remember_me)
+                    }),
+                    (SessionStatus::Unchanged, _, _) =>
                     // set a new session cookie upon first request (new client)
                     {
                         if is_new {
                             let state: HashMap<String, String> = HashMap::new();
                             res.checked_expr(|res| {
-                                inner.set_cookie(res, state.into_iter())
+                                inner.set_cookie(res, state.into_iter(), None)
                             })
                         } else {
                             res
                         }
                     }
-                    (SessionStatus::Purged, _) => {
+                    (SessionStatus::Purged, _, _) => {
                         let _ = inner.remove_cookie(&mut res);
                         res
                     }
@@ -396,6 +420,50 @@
     }
 
     #[actix_rt::test]
+    async fn dont_remember_me() {
+        let mut app = test::init_service(
+            App::new()
+                .wrap(CookieSession::signed(&[0; 32]).secure(false))
+                .service(
+                    web::resource("/")
+                        .to(|_ses: Session| async move { "test" }),
+                ),
+        )
+        .await;
+
+        let request = test::TestRequest::get().to_request();
+        let response = app.call(request).await.unwrap();
+        let cookie = response
+            .response()
+            .cookies()
+            .find(|c| c.name() == "actix-session")
+            .unwrap();
+        assert_eq!(cookie.max_age(), None);
+    }
+
+    #[actix_rt::test]
+    async fn remember_me() {
+        let mut app = test::init_service(
+            App::new()
+                .wrap(CookieSession::signed(&[0; 32]).secure(false))
+                .service(web::resource("/").to(|ses: Session| async move {
+                    ses.remember_me(true);
+                    "test"
+                })),
+        )
+        .await;
+
+        let request = test::TestRequest::get().to_request();
+        let response = app.call(request).await.unwrap();
+        let cookie = response
+            .response()
+            .cookies()
+            .find(|c| c.name() == "actix-session")
+            .unwrap();
+        assert_eq!(cookie.max_age(), Some(time::Duration::days(30)));
+    }
+
+    #[actix_rt::test]
     async fn private_cookie() {
         let mut app = test::init_service(
             App::new()
@@ -479,4 +547,56 @@
         let body = test::read_response(&mut app, request).await;
         assert_eq!(body, Bytes::from_static(b"counter: 100"));
     }
+
+    #[actix_rt::test]
+    async fn purge() {
+        let mut app = test::init_service(
+            App::new()
+                .wrap(
+                    CookieSession::signed(&[0; 32])
+                        .path("/test/")
+                        .name("actix-test")
+                        .domain("localhost")
+                        .http_only(true)
+                        .same_site(SameSite::Lax)
+                        .max_age(100),
+                )
+                .service(web::resource("/").to(|ses: Session| async move {
+                    let _ = ses.set("a", 1);
+                    "test"
+                }))
+                .service(web::resource("/p").to(|ses: Session| async move {
+                    ses.purge();
+                    "purged"
+                })),
+        )
+        .await;
+
+        let request = test::TestRequest::get().to_request();
+        let response = app.call(request).await.unwrap();
+        let cookie = response
+            .response()
+            .cookies()
+            .find(|c| c.name() == "actix-test")
+            .unwrap()
+            .clone();
+        assert_eq!(cookie.path().unwrap(), "/test/");
+
+        let request = test::TestRequest::with_uri("/p")
+            .cookie(cookie)
+            .to_request();
+        let response = app.call(request).await.unwrap();
+        let cookie = response
+            .response()
+            .cookies()
+            .find(|c| c.name() == "actix-test")
+            .unwrap()
+            .clone();
+        assert_eq!(cookie.path().unwrap(), "/test/");
+        assert_eq!(cookie.value(), "");
+        assert_eq!(cookie.max_age().unwrap(), time::Duration::seconds(0));
+        assert_eq!(cookie.domain().unwrap(), "localhost");
+        assert_eq!(cookie.http_only().unwrap(), true);
+        assert_eq!(cookie.same_site().unwrap(), SameSite::Lax);
+    }
 }
